function hoverImg(element, url) {
    element.setAttribute('src', url);
}

function unHoverImg(element, url) {
    element.setAttribute('src', url);
}

function onPhotoClick(id) {
    const value = document.getElementById(id);
    $('#imagePreview').attr('src', $(value).attr('src'));
    $('#imageModal').modal('show');
}

const page = window.location.pathname.split("/").pop().split(".")[0]
const url = "index" === page ? "html/image-modal.html" : "../html/image-modal.html"
fetch(url)
    .then(response => {
        return response.text()
    })
    .then(data => {
        document.getElementById("image-module").innerHTML = data;
    })